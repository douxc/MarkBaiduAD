/**
 * Created by xinchao.dou on 2016/5/9.
 */
const gulp = require('gulp');
const gulpUglify = require('gulp-uglify');
const gulpCopy = require('gulp-copy');


gulp.task('build', ['uglify', 'copy']);

gulp.task('uglify', function () {
    return gulp.src('./src/*.js').pipe(gulpUglify()).pipe(gulp.dest('./dist'));
});

gulp.task('copy', function () {
    return gulp.src(['./src/*.json', './src/*.html','./src/*.png']).pipe(gulpCopy('./dist', {prefix: 1}));
});