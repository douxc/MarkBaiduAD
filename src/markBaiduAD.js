"use strict";
var _backgroundColor = "rgba(51,133,255,0.3) !important"; //高亮的背景色
window.onload
    ? window.onload = markAD
    : document.addEventListener('ready', markAD);

if (/(baidu.com)/g.test(window.location.href)) {
    markAD();
}
/**
 * 标记广告内容
 */
function markAD() {
    var url = window.location.href;
    //百度首页搜索结果
    if (url.indexOf('www.baidu.com') > 0) {
        baiduSearch();
        setInterval(baiduSearch, 3000);
    }
    //百度贴吧推广
    if (url.indexOf('tieba.baidu.com') > 0) {
        tieba();
        setInterval(tieba, 3000);
    }
    //其他网站百度广告
    BaiduDspuiFlowbar();
}
/**
 * 百度搜索的结果
 */
function baiduSearch() {
    //右侧推广广告
    var targetElements = [];
    var rightAd = document.getElementById('ec_im_container');
    if (rightAd && (/(推广链接)/g.test(rightAd.innerHTML) || (/(商业推广)/g.test(rightAd.innerHTML)))) {
        targetElements.push(rightAd.parentNode);
    }
    //搜索结果推广链接类广告
    var searchResult = document.getElementById('content_left');
    if (!searchResult || null === searchResult) {
        //为空，此次不再处理
        return;
    }
    var elements = searchResult.querySelectorAll('a');

    for (var i = 0, len = elements.length; i < len; i++) {
        var element = elements[i];
        if (element.textContent === '推广链接' || element.textContent === '商业推广') {
            //targetElement.push(element);
            targetElements.push(element.parentNode);
        }
    }
    //搜索结果推广广告
    var divElements = searchResult.childNodes;
    for (var j = 0, _len = divElements.length; j < _len; j++) {
        var div = divElements[j];
        if ('querySelectorAll' in div && parseInt(div.id) > 3000) {
            var fonts = div.querySelectorAll('font');
            for (var k = 0, __len = fonts.length; k < __len; k++) {
                var _font = fonts[k];
                if (/(推广)/g.test(_font.innerHTML)) {
                    targetElements.push(div);
                }
            }
        }

    }
    targetElements.forEach(function (e) {
        e.style.cssText += 'background-color:' + _backgroundColor;
    });
}
/**
 * 贴吧推广
 */
function tieba() {
    var list = document.getElementById('thread_list'),
        targetElements = [];
    if (!list || null === list) {
        return;
    }
    var liElements = list.querySelectorAll('li');
    for (var i = 0, len = liElements.length; i < len; i++) {
        var _target = liElements[i];
        if (!/(thread_list)/g.test(_target.className) && /(推广)/g.test(_target.innerHTML)) {
            //class中没有thread_list;内容含有推广字样;可能是推广
            targetElements.push(_target);
        }
    }
    targetElements.forEach(function (e) {
        e.style.cssText += 'background-color:' + _backgroundColor;
    });
}

function BaiduDspuiFlowbar() {
    var targetElements = [];
    //jquery之家
    var htmleafAD = document.getElementById('BAIDU_DSPUI_FLOWBAR');
    if (htmleafAD) {
        targetElements.push(htmleafAD);
    }
    //极客头条详情页
    var ad = document.getElementsByClassName('detail_ad');
    for (var i = 0, len = ad.length; i < len; i++) {
        targetElements.push(ad[i]);
    }
    //处理
    targetElements.forEach(function (e) {
        e.parentNode.removeChild(e);
    });
}
